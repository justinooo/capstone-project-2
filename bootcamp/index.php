<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	<title></title>

	<link rel="shortcut icon" href="#" type="image/x-icon">
    <link rel="icon" href="#" type="image/x-icon">
	
	<link rel="stylesheet" href="/library/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/library/css/style.css">

</head>
<body>
			<div class="main">
				<div class="container">
					<div class="login col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="login__header">
								<h1>Welcome Please Log in!</h1>
								<p>OR</p>
								<a href="#">Sign up</a>
							</div>
							<div class="login__userlog">
								<input class="login__userlog--userinfo form-control" placeholder="Email" type="text" name="username">
								<input class="login__userlog--userinfo form-control" placeholder="Create a password" type="password" name="password">
								<button type="button" class="btn btn-danger btn-block"> <a href="home.php">CONTINUE</a></button>
							</div>
						</div>
					</div>
				</div>
			</div>
				

	<script src="/library/plugins/jquery/jquery-2.1.4.min.js"></script>
	<script src="/library/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="/library/js/global.js"></script>
</body>
</html>