<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	<title></title>

	<link rel="shortcut icon" href="#" type="image/x-icon">
    <link rel="icon" href="#" type="image/x-icon">
	
	<link rel="stylesheet" href="/library/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/library/css/style.css">
</head>
<body>

			<div class="site-header">
				<div class="logo"></div>
				
				<div class="locale"></div>
				<div class="account"></div>
				<div class="mobile-toggle">
					<i class="fa fa-bars mobile-toggle__icon" aria-hidden="true"></i>
				</div>
				<div class="site-nav">
					<ul class="site-nav__list">
						<li class="site-nav__item"><a href="#" class="site-nav__link">About</a></li>
						<li class="site-nav__item"><a href="#" class="site-nav__link">Products</a></li>
						<li class="site-nav__item"><a href="#" class="site-nav__link">Services</a></li>
						<li class="site-nav__item"><a href="#" class="site-nav__link">Business Owners</a></li>
						<li class="site-nav__item"><a href="#" class="site-nav__link">Apps</a></li>
						<li class="site-nav__item"><a href="#" class="site-nav__link">FAQ</a></li>
						<li class="site-nav__item"><a href="#" class="site-nav__link">Blog</a></li>
						<li class="site-nav__item"><a href="#" class="site-nav__link">Contact</a></li>
					</ul>
				</div>
			</div>
			<main>
				<div class="home">
					<div class="container">
						<div class="col-xs-12">
							<div class="homepage">
								<div class="col-xs-12 col-sm-12 col-md-12">
									 <div class="fb-profile">
								        <img align="left" class="fb-image-lg fb-profile__header" src="/library/images/header.jpg" alt="Profile image example"/>
								        <img align="left" class="fb-image-profile thumbnail fb-profile__prof" src="/library/images/user.jpg" alt="Profile image example"/>
								        <div class="fb-profile-text">
								            <h1>Eli Macy</h1>
								            <p>Girls just wanna go fun.</p>
								        </div>
								    </div>
								</div>
								<div class=" col-xs-12 col-sm-12 col-md-8 col-md-offset-4">
									<div class="user">
										<div class="user__post">
											<div class="user__post--header">
												<h4>Create a post</h4>
											</div>

											<div class="user__post--field">
												<div class="col-xs-6">
													<h3>Select jeepney</h3>
													<select name="" id="" class="form-control">
														<option class="form-control" value=""></option>
														<option class="form-control" value=""></option>
														<option class="form-control" value=""></option>
													</select>
												</div>
												<div class="col-xs-6">
													<h3>Rate of Jeepney</h3>
													<select name="" id="" class="form-control">
														<option value=""></option>
														<option value=""></option>
														<option value=""></option>
													</select>
												</div>
												<textarea name="" id="" cols="70" rows="5" placeholder="Write a post..."></textarea>
												<button class="btn btn-default">Post</button>
											</div>
										</div>
										<div class="user__line"></div>
										<div class="col-xs-12 user__viewpost">
											<div class=" col-xs-12 ">
													<div class="col-xs-4 col-sm-3 col-md-2 user__viewpost--images">
														<img src="/library/images/user.jpg" alt="">
													</div>
													<div class="user__viewpost--name">
														<div class="col-xs-6 col-sm-8 col-md-8">
															<h4>Juan Dela Cruz</h4>
														</div>
														<div class="col-xs-6 col-sm-8 col-md-8">
															<p>01/01/2017</p>
														</div>
													</div>
											</div>
											<div class=" col-xs-12 ">
												<div class="user__viewpost--details">
													<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 user__comments">
											<textarea name="" id="" placeholder="Write a comment"></textarea>
											<button class="btn btn-default">Comment</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<div class="mobile"></div>
			

	<script src="/library/plugins/jquery/jquery-2.1.4.min.js"></script>
	<script src="/library/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="/library/js/global.js"></script>
</body>
</html>