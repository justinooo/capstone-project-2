<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	<title></title>

	<link rel="shortcut icon" href="#" type="image/x-icon">
    <link rel="icon" href="#" type="image/x-icon">
	
	<link rel="stylesheet" href="/library/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/library/css/style.css">

</head>
<body>
			<div class="main">
				<div class="container">
					<div class="login col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="login__header">
								<h1>Registstion Form</h1>
							</div>
							<div class="login__userlog">
								<input class="login__userlog--userinfo form-control" placeholder="First Name" type="text" name="firstname">
								<input class="login__userlog--userinfo form-control" placeholder="Last Name" type="text" name="lastname">
								<input class="login__userlog--userinfo form-control" placeholder="Contact" type="text" name="contact">
								<select name="" id="" class="form-control">
										<option value="">Gender</option>
										<option value="">Male</option>
										<option value="">Female</option>
								</select>
								<input class="login__userlog--userinfo form-control" placeholder="Email" type="text" name="username">
								<input class="login__userlog--userinfo form-control" placeholder="Create a password" type="password" name="password">
								<button type="button" class="btn btn-danger btn-block"> <a href="home.php">Sign Up</a></button>
							</div>
						</div>
					</div>
				</div>
			</div>
				

	<script src="/library/plugins/jquery/jquery-2.1.4.min.js"></script>
	<script src="/library/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="/library/js/global.js"></script>
</body>
</html>